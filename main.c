#include "get_next_line.h"
#include <fcntl.h>
#include <stdio.h>

int			main(int ac, char **av) {
	int		ret;
	int		fd;
	char	*line;

	if (ac == 2)
		fd = open(av[1], O_RDONLY);
	else
		fd = 0;
	while ((ret = get_next_line(fd, &line)) > 0) {
		printf("%d -> %s\n", ret, line);
	}
	printf("RET : %d\n", ret);
	return (0);
}
