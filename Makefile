# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: triviere <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/01/12 16:21:12 by triviere          #+#    #+#              #
#    Updated: 2016/01/13 14:46:08 by triviere         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

all:
	make -C libft
	gcc -Wall -Werror -Wextra -L libft -I Libft/includes -lft *.c

clean:
	make clean -C libft

fclean: clean
	make fclean -C libft
	rm a.out

run:
	rm -rf result
	mkdir -p result result/arg result/stdin
	./a.out test/16_1 > result/arg/16_1
	./a.out test/16_2 > result/arg/16_2
	./a.out test/16_3 > result/arg/16_3
	./a.out test/16_4 > result/arg/16_4
	./a.out test/16_5 > result/arg/16_5
	./a.out test/8_1 > result/arg/8_1
	./a.out test/8_2 > result/arg/8_2
	./a.out test/8_3 > result/arg/8_3
	./a.out test/8_4 > result/arg/8_4
	./a.out test/8_5 > result/arg/8_5
	./a.out test/4_1 > result/arg/4_1
	./a.out test/4_2 > result/arg/4_2
	./a.out test/4_3 > result/arg/4_3
	./a.out test/4_4 > result/arg/4_4
	./a.out test/4_5 > result/arg/4_5
	cat test/16_1 | ./a.out > result/stdin/16_1
	cat test/16_2 | ./a.out > result/stdin/16_2
	cat test/16_3 |./a.out > result/stdin/16_3
	cat test/16_4 | ./a.out > result/stdin/16_4
	cat test/8_1 | ./a.out > result/stdin/8_1
	cat test/8_2 | ./a.out > result/stdin/8_2
	cat test/8_3 | ./a.out > result/stdin/8_3
	cat test/8_4 | ./a.out > result/stdin/8_4
	cat test/4_1 | ./a.out > result/stdin/4_1
	cat test/4_2 | ./a.out > result/stdin/4_2
	cat test/4_3 | ./a.out > result/stdin/4_3
	cat test/4_4 | ./a.out > result/stdin/4_4

correction:
	diff result/arg/16_1 diff/arg/16_1
	diff result/arg/16_2 diff/arg/16_2
	diff result/arg/16_3 diff/arg/16_3
	diff result/arg/16_4 diff/arg/16_4
	diff result/arg/16_5 diff/arg/16_5
	diff result/stdin/16_1 diff/stdin/16_1
	diff result/stdin/16_2 diff/stdin/16_2
	diff result/stdin/16_3 diff/stdin/16_3
	diff result/stdin/16_4 diff/stdin/16_4
	diff result/arg/8_1 diff/arg/8_1
	diff result/arg/8_2 diff/arg/8_2
	diff result/arg/8_3 diff/arg/8_3
	diff result/arg/8_4 diff/arg/8_4
	diff result/arg/8_5 diff/arg/8_5
	diff result/stdin/8_1 diff/stdin/8_1
	diff result/stdin/8_2 diff/stdin/8_2
	diff result/stdin/8_3 diff/stdin/8_3
	diff result/stdin/8_4 diff/stdin/8_4
	diff result/arg/4_1 diff/arg/4_1
	diff result/arg/4_2 diff/arg/4_2
	diff result/arg/4_3 diff/arg/4_3
	diff result/arg/4_4 diff/arg/4_4
	diff result/arg/4_5 diff/arg/4_5
	diff result/stdin/4_1 diff/stdin/4_1
	diff result/stdin/4_2 diff/stdin/4_2
	diff result/stdin/4_3 diff/stdin/4_3
	diff result/stdin/4_4 diff/stdin/4_4
